//Dustin Gallinger
/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)



// 2. What is the cost per square inch of each pizza?


// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)

const randomCard = Math.ceil(Math.random() * 13);

// 4. Draw 3 cards and use Math to determine the highest
// card

const cardOne = Math.ceil(Math.random() * 13);
const cardTwo = Math.ceil(Math.random() * 13);
const cardThree = Math.ceil(Math.random() * 13);

const maxVal = Math.max(cardOne, cardTwo, cardThree);

console.log(maxVal);

/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

const firstName = 'Dustin';
const lastName = 'Gallinger';
const streetAddress = '1234 Main Street';
const city = 'Seattle';
const state = 'WA';
const zipCode = '999999';

const combinedAddress = `${firstName} ${lastName}
${streetAddress}
${city} ${state} ${zipCode}`;

console.log(combinedAddress);

// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring

const firstSpace = combinedAddress.indexOf(' ', 0);

const nameFirst = combinedAddress.substring(0, firstSpace);

console.log(nameFirst);

/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

// Starting hint:

const endDate = new Date(2020, 3, 1, 0, 0, 0);
const startDate = new Date(2020, 0, 1, 0, 0, 0);


const midDate = new Date((endDate.valueOf() - startDate.valueOf()) / 2);

console.log(midDate);